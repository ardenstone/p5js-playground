var img;
var mouth;
var mic;

function setup() {
  mic = new p5.AudioIn()
  mic.start();
  createCanvas(windowWidth, windowHeight);
  img = loadImage("img/main_head.png");  // Load the main image
  mouth = loadImage("img/mouth.png");
}

function draw() {
  background(0);
  micLevel = mic.getLevel();
  micScale = windowWidth * .5;

  ratio = img.height / img.width;
  ratio2 = img.width / img.height;

  imgHeight = (windowWidth * ratio);
  imgWidth = windowWidth;
  xP = 0;

  if (imgHeight > windowHeight) {
    imgHeight = windowHeight;
    imgWidth = (windowHeight * ratio2);

    xP = (windowWidth / 2) - (imgWidth / 2);
    micScale = (windowHeight * ratio2) * .5;
  }
  image(img, xP, 0, imgWidth, imgHeight);
  image(mouth, xP, 0 + (micLevel * micScale), imgWidth, imgHeight);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
